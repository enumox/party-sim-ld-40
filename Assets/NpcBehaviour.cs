﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.AI;

public class NpcBehaviour : MonoBehaviour {

	[Header("Prefabs")]
	[SerializeField] 
	private GameObject _bubblePrefab;

	[Header("Audio")] 
	[SerializeField]
	private AudioClip _audioDrink;
	
	[SerializeField]
	private AudioClip _audioFood;
	
	[Header("Movement")]
	[SerializeField] 
	private float _minSpeed = 4;
	
	[SerializeField] 
	private float _maxSpeed = 7;
	
	[SerializeField] 
	private float _fightDistance = 4f;

	[Header("Factors")]
	[SerializeField] 
	private float _hungerIncreaseFactor = 0.005f;

	[SerializeField] 
	private float _thirstIncreaseFactor = 0.005f;

	[SerializeField] 
	private float _lookForConsumablesLevel = 0.5f;

	[SerializeField] 
	private float _minWaitTimeToMove = 5f;
	
	[SerializeField] 
	private float _maxWaitTimeToMove = 10f;
	
	[SerializeField] 
	private float _startFightChance = 0.005f;
	
	//TODO: Remove follow player
	private float _speed;
	private NavMeshAgent _meshAgent;
	private Consumables.DrinkType _myDrink; 
	private Consumables.FoodType _myFood;
	
	private float _hunger = 0;
	private float _thirst = 0;
	private float _newTimeInterval;
	private GameObject _currentBubble;
	private GameObject _fightingWith;
	private bool _cantFight = false;
	private bool _dead = false;
	private AudioSource _audio;
	
	private bool _spawnedBubble = false;
	[HideInInspector]
	public bool isAskingForFoodOrDrink = false;
	[HideInInspector]
	public bool isFighting = false;
	[HideInInspector]
	public int id;
	
	[HideInInspector]
	public float happiness = 1000f;

	[HideInInspector] 
	public bool isIntruder = false;
	
	[HideInInspector] 
	public bool beingDragged = false;
	
	
	void Start() {
		id = Random.Range(0, 999999);
		_thirst = Random.Range(0.2f, 0.5f);
		_hunger = Random.Range(0.2f, 0.5f);
		isIntruder = Random.Range(1, 1000) > 900;
		if (isIntruder) {
			print("i'm intruder");
		}
		print(_thirst);
		_speed = Random.Range(_minSpeed, _maxSpeed);
		_meshAgent = GetComponent<NavMeshAgent>();
		_audio = GetComponent<AudioSource>();
		_meshAgent.speed = _speed;
		_myDrink = Consumables.GetRandomDrinkType();
		_myFood = Consumables.GetRandomFoodType();
		_hungerIncreaseFactor = Random.Range(_hungerIncreaseFactor, _hungerIncreaseFactor + _hungerIncreaseFactor / 2f);
		_thirstIncreaseFactor = Random.Range(_thirstIncreaseFactor, _thirstIncreaseFactor + _thirstIncreaseFactor / 2f);
		LookForNewPlaceToGo();		
	}

	void Update() {
		if (_dead || beingDragged) {
			return;
		}
		if (isFighting) {
			FightingBehaviour();
			return;
		}
		if (!_cantFight) {
			CheckStartFight();
		}
		IncreaseHungerAndThirst();
		CheckHungerAndThirst();
		CalculateHappiness();
		if (Time.time > _newTimeInterval) {
			LookForNewPlaceToGo();
		}
		
	}


	private void IncreaseHungerAndThirst() {
		_hunger += _hungerIncreaseFactor * Time.deltaTime;
		_thirst += _thirstIncreaseFactor * Time.deltaTime;
	}

	private void CheckHungerAndThirst() {
		if (_thirst > _lookForConsumablesLevel) {
			if (!_spawnedBubble) {
				SpawnBubble(true);
				isAskingForFoodOrDrink = true;
			}
			if (_thirst >= 1f) {
				isAskingForFoodOrDrink = false;
				LeavePartyAndBreakStuff();
			}
		}

		if (_hunger > _lookForConsumablesLevel) {
			if (!_spawnedBubble) {
				SpawnBubble(false);
				isAskingForFoodOrDrink = true;
			}
			if (_hunger >= 1f) {
				isAskingForFoodOrDrink = false;
				LeavePartyAndBreakStuff();
			}
		}
	}

	private void LeavePartyAndBreakStuff() {
		Debug.Log("I should be breaking shit up");
	}

	private void SpawnBubble(bool isDrink) {
		_spawnedBubble = true;
		_currentBubble = Instantiate(_bubblePrefab);
		var bubbleScript = _currentBubble.GetComponent<Bubble>();
		bubbleScript.isDrink = isDrink;
		if (isDrink) {
			bubbleScript.drinkType = _myDrink;
		} else {
			bubbleScript.foodType = _myFood;
		}
		bubbleScript.follow = transform;
	}

	public bool GiveFood(Consumables.FoodType food) {
		if (food != _myFood) {
			//TODO: Remove happines?
			return false;
		}
		isAskingForFoodOrDrink = false;
		_hunger -= 0.5f;
		if (_hunger < 0) {
			_hunger = 0;
		}
		Destroy(_currentBubble);
		_spawnedBubble = false;
		_audio.clip = _audioFood;
		_audio.Play();
		return true;
	}

	private void LookForNewPlaceToGo() {
		_newTimeInterval = Time.time + Random.Range(_minWaitTimeToMove, _maxWaitTimeToMove);
		//find place to go
		var newZ = Random.Range(-7f, 30f);
		var newX = Random.Range(-30f, 30f);
		_meshAgent.destination = new Vector3(newX, 0, newZ);
	}

	private void CalculateHappiness() {
		happiness -= (_thirst + _hunger) * Time.deltaTime * 5f;
	}

	private void CheckStartFight() {
		if (isFighting) {
			return;
		}
		if (_hunger > 0.6f || _thirst > 0.6f) {
			print("I'll pick a fight mate");
			var min = 10000 - _startFightChance;
			if (Random.Range(0, 10000) > min) {
				var npcs = FindObjectsOfType<NpcBehaviour>().Where(x => x.isFighting == false && x.id != id).ToList();
				if (!npcs.Any()) {
					_cantFight = true;
					return;
				}
				if (_spawnedBubble) {
					Destroy(_currentBubble);
					_spawnedBubble = false;
				}
				_spawnedBubble = true;
				_currentBubble = Instantiate(_bubblePrefab);
				var bubbleScript = _currentBubble.GetComponent<Bubble>();
				bubbleScript.isSkull = true;
				bubbleScript.follow = transform;
				_fightingWith = npcs[Random.Range(0, npcs.Count)].gameObject;
				_fightingWith.GetComponent<NpcBehaviour>().PickAFight(gameObject);
				isFighting = true;
			}
		}
	}

	private void FightingBehaviour() {
		_meshAgent.destination = _fightingWith.transform.position;
		if (Vector3.Distance(transform.position, _fightingWith.transform.position) <= _fightDistance) {
			
		}
	}

	public void Drag(GameObject dragger) {
		var distance = 1.5f;
		var position = dragger.transform.position - (-dragger.transform.forward * distance);
		transform.position = position;
		beingDragged = true;

	}

	public void PickAFight(GameObject starer) {
		isFighting = true;
		_fightingWith = starer;
	}
	
	public bool GiveDrink(Consumables.DrinkType drink) {
		if (drink != _myDrink) {
			//TODO: Remove happines?
			return false;
		}
		isAskingForFoodOrDrink = false;
		_thirst -= 0.5f;
		if (_thirst < 0) {
			_thirst = 0;
		}
		Destroy(_currentBubble);
		_spawnedBubble = false;
		_audio.clip = _audioDrink;
		_audio.Play();
		return true;
	}

	public void GetNockedOut(Transform player, float force) {
		print("Get knocked out");
		isFighting = false;
		GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Rigidbody>().AddForce(transform.position - player.position  * force, ForceMode.Impulse);
		Destroy(_currentBubble);
		FindObjectOfType<GameManager>().numberOfGuests--;
		FindObjectOfType<GameManager>().GenerateMoreGuests();
		_dead = true;
		Destroy(_meshAgent);
		Destroy(this);
	}

	public void DestroyBubble() {
		Destroy(_currentBubble);
	}
}
