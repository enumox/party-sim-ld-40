﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class FoodPlate : MonoBehaviour {

	[SerializeField] 
	private Consumables.FoodType _type;


	private string _actionMessage = "";
	private Text _actionDisplayText;

	void Start() {
		_actionDisplayText = GameObject.FindGameObjectWithTag("Action Display")
			.transform.GetChild(0).GetComponent<Text>();
		_actionMessage = "Press A(JoyPad) or Space to take one " + _type;
	}

	private void OnTriggerEnter(Collider other) {
		if (!other.gameObject.CompareTag("Player")) {
			return;
		}
		if (!_actionDisplayText.text.Contains(_actionMessage)) {
			_actionDisplayText.text += "\n" + _actionMessage;
		}		
	}

	private void OnTriggerStay(Collider other) {
		if (!other.gameObject.CompareTag("Player")) {
			return;
		}

		var pc = other.gameObject.GetComponent<PlayerController>();
		
		if (Input.GetKeyDown("joystick button 0") || Input.GetKeyDown(KeyCode.Space)) {
			pc.StartCarryingFood(_type);
		}
		
	}

	private void OnTriggerExit(Collider other) {
		if (!other.gameObject.CompareTag("Player")) {
			return;
		}
		if (_actionDisplayText.text.Contains(_actionMessage)) {
			_actionDisplayText.text =  _actionDisplayText.text.Replace("\n" + _actionMessage, "");
		}
	}
}
