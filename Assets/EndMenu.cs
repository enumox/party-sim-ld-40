﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMenu : MonoBehaviour {

	[SerializeField] 
	private Text _score;


	private void OnEnable() {
		GameObject.FindGameObjectWithTag("Action Display").SetActive(false);
		_score.text = FindObjectOfType<GameManager>().GetFinalScore() + "";

	}
}
