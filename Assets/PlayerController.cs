﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	[Header("UI")] 
	[SerializeField]
	private Text _carryingText;

	[Header("Prefabs")] 
	[SerializeField] 
	private GameObject _bottlePreab;
	[SerializeField] 
	private GameObject _hamburgerPrefab;
	[SerializeField] 
	private GameObject _pizzaPrefab;
	[SerializeField] 
	private GameObject _burritoPrefab;
	[Header("Info")]
	[SerializeField] 
	private float _speed;

	[SerializeField] 
	private int _strength;


	private bool _isCarrying = false;
	private bool _carryingFood = false;
	private bool _carryingDrink = false;

	private Consumables.DrinkType _currentDrink;
	private Consumables.FoodType _currentFood;
	private Text _actionDisplayText;
	private Rigidbody _rb;
	private string _actionText = "\nPress A(JoyPad) or Space to hand drink or food";
	private string _intruderText = "\nPress B(JoyPad) or X to grab the intruder";
	private string _fightText = "\nPress A(JoyPad) or Space punch tem in the face and break the fight up";
	private float _lastPickupTime;

	
	void Start() {
		_lastPickupTime = Time.time;
		_rb = GetComponent<Rigidbody>();
		_actionDisplayText = GameObject.FindGameObjectWithTag("Action Display")
			.transform.GetChild(0).GetComponent<Text>();
		print(_actionDisplayText == null);
	}


	private void FixedUpdate() {
		Movement();
	}

	private void Update() {
		if (Input.GetKeyDown("joystick button 3") || Input.GetKeyDown(KeyCode.Z)) {
			DropCarrying();
		}
	}

	private void Movement() {
		//Point towards where moving
		var horizontal = Input.GetAxisRaw("Horizontal");
		var vertical = Input.GetAxisRaw("Vertical");
		var movementNormal = new Vector3(horizontal, 0, vertical).normalized;
		_rb.velocity = movementNormal * _speed;
	}

	public void StartCarryingDrink(Consumables.DrinkType drink) {
		if (_isCarrying) {
			return;
		}
		_lastPickupTime = Time.time;
		_isCarrying = true;
		_carryingDrink = true;
		_currentDrink = drink;
		UpdateCarryingText("Now Carrying: " + drink);
	}

	public void StartCarryingFood(Consumables.FoodType food) {
		if (_isCarrying) {
			return;
		}
		_lastPickupTime = Time.time;
		_carryingFood = true;
		_isCarrying = true;
		_currentFood = food;
		UpdateCarryingText("Now Carrying: " + food);
	}

	private void DropCarrying() {
		if (Time.time < _lastPickupTime + 0.5f) {
			return;
		}
		if (!_isCarrying) {
			return;
		}

		if (_carryingDrink) {
			Instantiate(_bottlePreab, transform.position, Quaternion.identity);
			print("Drop bottle");
		} else {
			switch(_currentFood) {
				case Consumables.FoodType.Burrito:
					Instantiate(_burritoPrefab, transform.position, Quaternion.identity);
					break;
				case Consumables.FoodType.Hamburger:
					Instantiate(_hamburgerPrefab, transform.position, Quaternion.identity);
					break;
				case Consumables.FoodType.Pizza:
					Instantiate(_pizzaPrefab, transform.position, Quaternion.identity);
					break;
				
			}
		}
		_isCarrying = false;
		_carryingDrink = false;
		_carryingFood = false;
		UpdateCarryingText("Now Carrying: Nothing");
	}


	private void UpdateCarryingText(string text) {
		_carryingText.text = text;
	}

	private void HandCarriedItem(NpcBehaviour npc) {
		if (!_isCarrying) { 
			return;
		}

		if (_carryingDrink) {
			if (npc.GiveDrink(_currentDrink)) {
				_isCarrying = false;
				_carryingDrink = false;
				UpdateCarryingText("Now Carrying: Nothing");
			}
		} else {
			if (npc.GiveFood(_currentFood)) {
				_isCarrying = false;
				_carryingFood = false;
				UpdateCarryingText("Now Carrying: Nothing");
			}		
		}
	}

	private void OnTriggerEnter(Collider other) {
		var npc = other.gameObject.GetComponent<NpcBehaviour>();
		if (npc == null) {
			return;
		}
		if (npc.isAskingForFoodOrDrink) {
			ShowActionText();
		}
		if (npc.isFighting) {
			ShowFightText();
		}
		if (npc.isIntruder) {
			ShowIntruderText();
		}
	}
	
	private void OnTriggerStay(Collider other) {
		var npc = other.gameObject.GetComponent<NpcBehaviour>();
		if (npc == null) {
			return;
		}

		
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("joystick button 0")) {
			if (npc.isFighting) {
				npc.GetNockedOut(transform, _strength);
			} else {
				HandCarriedItem(npc);
			}
			
		}
		
		if (Input.GetKey(KeyCode.X) || Input.GetKey("joystick button 1")) {
			if (npc.isIntruder) {
				npc.Drag(gameObject);
			}
			
		}
	}
	
	

	private void OnTriggerExit(Collider other) {
		var npc = other.gameObject.GetComponent<NpcBehaviour>();
		if (npc == null) {
			return;
		}
		
		npc.beingDragged = false;
		RemoveActionText();
		RemoveIntruderText();
		RemoveFightText();
	}

	private void ShowActionText() {
		if (!_actionDisplayText.text.Contains(_actionText)) {
			_actionDisplayText.text += _actionText;
		}
	}

	private void ShowIntruderText() {
		if (!_actionDisplayText.text.Contains(_intruderText)) {
			_actionDisplayText.text += _intruderText;
		}
	}

	private void ShowFightText() {
		if (!_actionDisplayText.text.Contains(_fightText)) {
			_actionDisplayText.text += _fightText;
		}
	}

	private void RemoveActionText() {
		_actionDisplayText.text = _actionDisplayText.text.Replace(_actionText, "");
	}
	
	private void RemoveIntruderText() {
		_actionDisplayText.text = _actionDisplayText.text.Replace(_intruderText, "");
	}
	
	private void RemoveFightText() {
		_actionDisplayText.text = _actionDisplayText.text.Replace(_fightText, "");
	}
}
