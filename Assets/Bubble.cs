﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Bubble : MonoBehaviour {

	[HideInInspector] public bool isDrink = false;
	[HideInInspector] public bool isSkull = false;
	[HideInInspector] public Consumables.DrinkType drinkType;
	[HideInInspector] public Consumables.FoodType foodType;
	[HideInInspector] public Transform follow;


	[SerializeField] 
	private Vector3 _offSet;
	
	[SerializeField]
	private Sprite _beerSprite;
	[SerializeField]
	private Sprite _ginSprite;
	[SerializeField]
	private Sprite _vodkaSprite;
	[SerializeField]
	private Sprite _whiskySprite;
	
	[SerializeField]
	private Sprite _pizzaSprite;
	[SerializeField]
	private Sprite _hamburgerSprite;
	[SerializeField]
	private Sprite _burritoSprite;
	
	[SerializeField]
	private Sprite _skullSprite;

	private SpriteRenderer _itemSprite;
	void Start() {
		_itemSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
		if (isSkull) {
			_itemSprite.sprite = _skullSprite;
			return;
		}
		switch (isDrink) {
			case true:
				switch (drinkType) {
					case Consumables.DrinkType.Beer:
						_itemSprite.sprite = _beerSprite;
						break;
					case Consumables.DrinkType.Vodka:
						_itemSprite.sprite = _vodkaSprite;
						break;
					case Consumables.DrinkType.Whisky:
						_itemSprite.sprite = _whiskySprite;
						break;
					case Consumables.DrinkType.Gin:
						_itemSprite.sprite = _ginSprite;
						break;
				}
			break;
			case false:
				switch (foodType) {
					case Consumables.FoodType.Pizza:
						_itemSprite.sprite = _pizzaSprite;
						break;
					case Consumables.FoodType.Burrito:
						_itemSprite.sprite = _burritoSprite;
						break;
					case Consumables.FoodType.Hamburger:
						_itemSprite.sprite = _hamburgerSprite;
						break;
				}
			break;
		}
	}

	void Update() {
		transform.position = follow.transform.position + _offSet;
	}
}
