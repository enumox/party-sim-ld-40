﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntruderDropZone : MonoBehaviour {
	
	private GameManager _gameManager;
	
	void Start() {
		_gameManager = FindObjectOfType<GameManager>();
	}
	private void OnTriggerEnter(Collider other) {
		var npc = other.gameObject.GetComponent<NpcBehaviour>();
		if (npc == null || !npc.isIntruder || !npc.beingDragged) {
			return;
		}
		_gameManager.intrudersExpeled++;
		npc.DestroyBubble();
		_gameManager.GenerateMoreGuests();
		Destroy(other.gameObject);
	}
}
