﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class Fridge : MonoBehaviour {

	[SerializeField]
	private GameObject _fridgeMenu;
	
	[SerializeField]
	private float _distanceToOpenMenu;

	private GameObject _player;

	private Text _actionDisplayText;

	private float _timeToOpenFridge = 0.5f;
	private float _lastTimeFridgeClosed;
	
	void Start() {
		_lastTimeFridgeClosed = Time.time;
		_fridgeMenu.SetActive(false);
		_player = GameObject.Find("Player");
		_actionDisplayText = GameObject.FindGameObjectWithTag("Action Display")
			.transform.GetChild(0).GetComponent<Text>();
		_lastTimeFridgeClosed = Time.time;
	}
	
	
	void Update() {
		CheckItem();
		if (Time.time > _lastTimeFridgeClosed + _timeToOpenFridge) {
			CheckFridge();
		}
		
	}

	private void CheckFridge() {
		if (!_fridgeMenu.activeSelf) {
			if (Vector3.Distance(transform.position, _player.transform.position) <= _distanceToOpenMenu) {
				if (!_actionDisplayText.text.Contains("Press A(JoyPad) or Space to open the fridge")) {
					_actionDisplayText.text += "\nPress A(JoyPad) or Space to open the fridge";
				}
				if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("joystick button 0")) {
					_fridgeMenu.SetActive(true);
					_lastTimeFridgeClosed = Time.time;
				}
			}
			else {
				if (_actionDisplayText.text.Contains("\nPress A(JoyPad) or Space to open the fridge")) {
					_actionDisplayText.text = _actionDisplayText.text.Replace("\nPress A(JoyPad) or Space to open the fridge", "");
				}
			}
		}
	}

	private void CheckItem() {
		if (!_fridgeMenu.activeSelf) {
			return;
		}
		var pc = _player.GetComponent<PlayerController>();
		
		if (Input.GetKeyDown("joystick button 0") || Input.GetKeyDown(KeyCode.Alpha1)) {
			pc.StartCarryingDrink(Consumables.DrinkType.Vodka);
			_fridgeMenu.SetActive(false);
			_lastTimeFridgeClosed = Time.time;
		}
		
		if (Input.GetKeyDown("joystick button 1") || Input.GetKeyDown(KeyCode.Alpha4)) {
			pc.StartCarryingDrink(Consumables.DrinkType.Beer);
			_fridgeMenu.SetActive(false);
			_lastTimeFridgeClosed = Time.time;
		}
		
		if (Input.GetKeyDown("joystick button 2")|| Input.GetKeyDown(KeyCode.Alpha2)) {
			pc.StartCarryingDrink(Consumables.DrinkType.Gin);
			_fridgeMenu.SetActive(false);
			_lastTimeFridgeClosed = Time.time;
		}
		
		if (Input.GetKeyDown("joystick button 3") || Input.GetKeyDown(KeyCode.Alpha3)) {
			pc.StartCarryingDrink(Consumables.DrinkType.Whisky);
			_fridgeMenu.SetActive(false);
			_lastTimeFridgeClosed = Time.time;
		}
	}
}
