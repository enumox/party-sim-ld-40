﻿using System;
using Random = UnityEngine.Random;

namespace DefaultNamespace {
    public static class Consumables {
        public enum DrinkType {
            Beer,
            Vodka,
            Whisky,
            Gin,
        }

        public enum FoodType {
            Burrito,
            Hamburger,
            Pizza
        }

        public static FoodType GetRandomFoodType() {
            var values = Enum.GetValues(typeof(FoodType));
            return (FoodType) values.GetValue(Random.Range(0, values.Length));
        }

        public static DrinkType GetRandomDrinkType() {
            var values = Enum.GetValues(typeof(DrinkType));
            return (DrinkType) values.GetValue(Random.Range(0, values.Length));
        }
    }
}