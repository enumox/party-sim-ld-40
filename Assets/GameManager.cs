﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	[Header("Game Info")] 
	[SerializeField]
	private int _playMinutes;
	
	[SerializeField]
	private int _minGuests = 4;
	
	[SerializeField]
	private int _maxGuests = 12;

	[Header("UI")] 
	[SerializeField] 	
	private Text _currentTime;
	[SerializeField] 	
	private Image _happinessMeter;
	
	[SerializeField] 	
	private GameObject _endMenu;
	
	[SerializeField]
	private Text _endPartyTime;

	[Header("Prefabs")]
	[SerializeField] 
	private GameObject _npcPrefabs;

	private float _playedTime = 0;
	private int _currentGuests;
	private int _maxHappiness;
	private float _happinessLevel;
	private List<NpcBehaviour> _npcs;
	
	[HideInInspector]
	public int numberOfGuests;
	
	[HideInInspector]
	public int intrudersExpeled = 0;

	void Start() {
		Time.timeScale = 1;
		_npcs = new List<NpcBehaviour>();
		_endPartyTime.text = "Party Ends: 0" + _playMinutes + ":00";
		for (int i = 0; i < Random.Range(_minGuests, _maxGuests); i++) {
			numberOfGuests++;
			var newZ = Random.Range(-7f, 30f);
			var newX = Random.Range(-30f, 30f);
			var pos = new Vector3(newX, _npcPrefabs.transform.position.y, newZ);
			var go = Instantiate(_npcPrefabs, pos, Quaternion.identity);
			_npcs.Add(go.GetComponent<NpcBehaviour>());
		}
	}

	void Update() {
		UpdateTimeText();
		CalculateHappiness();

		if (_playedTime >= _playMinutes * 60) {
			//end game and show score with retry button
			Time.timeScale = 0;
			_endMenu.SetActive(true);
		}
	}

	private void CalculateHappiness() {
		_maxHappiness = _npcs.Count * 1000;
		_happinessLevel = 0;
		foreach (var npc in _npcs) {
			_happinessLevel += npc.happiness;
		}

		_happinessLevel = _happinessLevel / _maxHappiness;
		_happinessMeter.fillAmount = _happinessLevel;
	}

	public int GetFinalScore() {
		_maxHappiness = _npcs.Count * 1000;
		_happinessLevel = 0;
		foreach (var npc in _npcs) {
			_happinessLevel += npc.happiness;
		}
		_happinessLevel = _happinessLevel / _maxHappiness;


		return (int) (30000 * _happinessLevel + intrudersExpeled * 1000);
	}
	
	private void UpdateTimeText() {
		_playedTime += Time.deltaTime;
		var minutes = (int)_playedTime / 60;
		var seconds = _playedTime - minutes * 60;
		_currentTime.text = "Current Time: 0" + minutes + ":" + seconds.ToString("0");
	}

	public void GenerateMoreGuests() {
		for (int i = 0; i < Random.Range(1, _maxGuests - numberOfGuests); i++) {
			numberOfGuests++;
			var newZ = Random.Range(-7f, 30f);
			var newX = Random.Range(-30f, 30f);
			var pos = new Vector3(newX, _npcPrefabs.transform.position.y, newZ);
			var go = Instantiate(_npcPrefabs, pos, Quaternion.identity);
			_npcs.Add(go.GetComponent<NpcBehaviour>());
		}
	}




}
